import tkinter as tk
from Equation import Expression

def Input_Seperations(inp):
    equations = inp.split(',')
    return equations
    
def equations_to_latex(event):
    equations = Input_Seperations(type_space.get())
    letex_expressions=[]
    for eq in equations:
        fn = Expression(str(eq))
        letex_expressions.append(fn)
    
    PrintSpace.delete(1.0,tk.END)
    for eq in letex_expressions:
        PrintSpace.insert(tk.END,str(eq)+'\n\n')

window=tk.Tk()
window.title('TypedToLaTeX')

instructions=tk.Label(window,justify='left',text='INSTRUCTIONS: \n Type the equations to be converted seperated with a comma in the entry area. \n Then press the Convert button')
instructions.grid(sticky='w',pady=(0,10),row=0, columnspan=3)

TextLabel=tk.Label(window,text='Type Equation:')
TextLabel.grid(sticky="w",pady=(0,10),column=0,row=2)

type_space = tk.Entry(window)
type_space.grid(sticky='w',column=1,row=2,pady=(0,10))

Browse_CSV_button = tk.Button(window,text='Convert')
Browse_CSV_button.grid(sticky='w',row=2,column=2,pady=(0,10))
window.bind('<Button-1>',equations_to_latex)
window.bind('<Return>', equations_to_latex)

PrintSpace=tk.Text(window)
PrintSpace.grid(columnspan=3)

window.mainloop()