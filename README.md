## Description
A python program able to convert typed equation expressions into LaTeX expressions.

## Requirements
*  Python 3.x
*  Tkinter module 
*  Equation 1.2.01 module (https://pypi.org/project/Equation/)

## Usage

1.  Open a terminal inside the scipts folder.
2.  Type `python typed_to_latex.py` if you have only python3 installed or `pyhton3 typed_to_latex.py` if you have both python2 and python3.
3.  Press the Enter key and the programs graphical interface will appear.

![](images/Program_GUI.png)


4.  Type an equation in the type equation entry box. If more that one equation are tyoed they need to be seperated with comma.
5.  Press the enter button or the Enter key and the LaTeX espressions will appear in the white space.

## Installation

1.  Download the repository with the download button or Clone it.
2.  Unzip the files.